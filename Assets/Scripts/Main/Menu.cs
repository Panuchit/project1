﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void StartGame()
    {
        SceneManager.LoadScene("GameLaser");
    }
    public void Select()
    {

        SceneManager.LoadScene("SelectGun");
    }
    public void Exit()
    {
        Application.Quit();

    }
    public void MenuGame()
    {
        SceneManager.LoadScene("Menu");
    }
}
