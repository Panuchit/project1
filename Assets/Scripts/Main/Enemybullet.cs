﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemybullet : MonoBehaviour
{
    [SerializeField] float speed;
    Rigidbody2D bullet;
    private void Start()
    {
        bullet = GetComponent<Rigidbody2D>();
        bullet.velocity = new Vector2(0, -speed);
    }
    
}
