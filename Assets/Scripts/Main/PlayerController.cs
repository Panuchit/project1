﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


    public class PlayerController : MonoBehaviour
    { 
        private Rigidbody2D rb;
        private Physics2D physics2D;
         public float speed;
        private float inmove;
        private int Heath = 100;
        private Vector2 moveDirection;
         public Text healthText;
       public AudioSource HP;
        public Slider sliderHp;
        public GameObject shoot;
        public Transform shootSpawn;
         public int shootdamage;
    
        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            sliderHp.maxValue = Heath;
            sliderHp.value = Heath;
        }

        public void Update()
        {
            ProcessInputs();
            healthText.text = "HEALTH: " + Heath;

            sliderHp.value = Heath;

            if (Heath < 1)
            {
                Heath = 0;
                Destroy(gameObject);
                SceneManager.LoadScene("YouLose");
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Instantiate(shoot, shootSpawn.position, shootSpawn.rotation);
            }
        }


        private void FixedUpdate()
        {
            Move();
        }
        void ProcessInputs()
        {
            float moveX = Input.GetAxisRaw("Horizontal");
            float moveY = Input.GetAxisRaw("Vertical");

            moveDirection = new Vector2(moveX, moveY).normalized;
        }

        private void Move()
        {
            rb.velocity = new Vector2(moveDirection.x * speed, moveDirection.y * speed);

        }


        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.tag == "HP")
            {
                Heath = Heath + 50;
                HP.Play();
                Destroy(other.gameObject);
            }
            if (other.gameObject.tag == "HP1")
            {
                Heath = Heath + 50;
                HP.Play();
                Destroy(other.gameObject);
            }
            if (other.gameObject.tag == "HP2")
            {
                Heath = Heath + 50;
                HP.Play();
                Destroy(other.gameObject);
            }
            if (other.gameObject.tag == "HP3")
            {
                Heath = Heath + 50;
                HP.Play();
                Destroy(other.gameObject);
            }
            if (other.gameObject.tag == "HP4")
            {
                Heath = Heath + 50;
                HP.Play();
                Destroy(other.gameObject);
            }
            if (other.gameObject.tag == "HP5")
            {
                Heath = Heath + 50;
                HP.Play();
                Destroy(other.gameObject);
            }
            if (other.gameObject.tag == "HP6")
            {
                Heath = Heath + 50;
                HP.Play();
                Destroy(other.gameObject);
            }

            if (other.gameObject.tag == "HP6")
            {
                Heath = Heath + 50;
                HP.Play();
                Destroy(other.gameObject);
            }

        if (other.gameObject.tag == "Enemybullet")
        {
            Heath = Heath - 20;
        }
    }
    

}

